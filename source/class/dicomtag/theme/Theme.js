/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("dicomtag.theme.Theme",
{
  meta :
  {
    color : dicomtag.theme.Color,
    decoration : dicomtag.theme.Decoration,
    font : dicomtag.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : dicomtag.theme.Appearance
  }
});
