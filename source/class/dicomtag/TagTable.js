/**
 * Create the table for the list of tags
 */
qx.Class.define("dicomtag.TagTable",
{
  extend : qx.core.Object,
  events : {
    /** 
     * Fired when the user selects the checkbox. I think this is unused.
     */
    "selectTag" : "qx.event.type.Data",
    /**
     * Fired when any of the tags have been (un)checked.
     */
    "tagToggled" : "qx.event.type.Data"
  },

  members : {
    /**
     * Create the table model for the tag table.
     * @param tags{String} A list of tags eg. ["0008,0012", "0012,0024" ...]
     */
    createModel : function(tags) {
      var that = this;
      var model = new qx.ui.table.model.Simple();
      model.setColumns(["","Description", "Tag"], ["visible", "Description", "Tag"]);
      var rowData = [];
      for (var i = 0; i < tags.length; i++) {
	var visible = false;
	if (qx.lang.Array.contains(dicomtag.Tags.defaultVisibleTags, tags[i])) {
	   visible = true;
	}
	rowData.push([visible, dicomtag.Tags.tags[tags[i]], tags[i]]);
      }
      model.setData(rowData);
      model.setColumnEditable(0, true);
      model.addListener("dataChanged", function (evt) {
			  var changed = evt.getData();
			  var rowId = changed.firstRow;
			  var visible = model.getRowDataAsMap(rowId)["visible"];
			  var tag = model.getRowDataAsMap(rowId)["Tag"];
			  that.fireDataEvent("tagToggled", {visible : visible, tag : tag});
			});
      return model;
    },
    /**
     * Create the table given the model
     * @param model{Object} The given model 
     */
    createPublicTable : function (model) {
      var table = new qx.ui.table.Table(model);
      table.getTableColumnModel().setDataCellRenderer(0, new qx.ui.table.cellrenderer.Boolean());
      table.getTableColumnModel().setColumnWidth(0, 25);
      table.getTableColumnModel().setCellEditorFactory(0, new qx.ui.table.celleditor.CheckBox());
      var that = this;
      table.addListener("cellClick", function (evt) {
			  var rowId = evt.getRow();
			  var tag = model.getRowDataAsMap(rowId)["Tag"];
			  that.fireDataEvent("selectTag", {
					       row : rowId,
					       tag : tag
					     });
			  var column = evt.getColumn();
			  if(column==0){
				var current = table.getTableModel().getValue(0,rowId);
			    table.getTableModel().setValue(0,rowId,!current);
			  }
			});
      // double clicking anywhere else on the row selects it
      table.addListener("cellDblclick", function (e){
			  var row = e.getRow();
			  var current = table.getTableModel().getValue(0,row);
			  table.getTableModel().setValue(0,row,!current);
			},this);
      return table;
    }
  }
});

